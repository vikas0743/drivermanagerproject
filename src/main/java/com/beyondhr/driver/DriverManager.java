package com.beyondhr.driver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverManager {

	private static WebDriver driver = null;
	private static Properties prop = null;

//	private WebDriver driver;

	synchronized public static WebDriver getDriver() {
		try {
			InputStream input = new FileInputStream("resources/config.properties");
			prop = new Properties();
			prop.load(input);
			String browser = prop.getProperty("browser");
			switch (browser) {
			case "chrome":
				if (driver == null) {
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver();
				}
				return driver;
			case "firefox":
				if (driver == null) {
					
					WebDriverManager.firefoxdriver().setup();
				}
				return driver;
			case "headless":
				if (driver == null) {
					driver = new ChromeDriver(new ChromeOptions().setHeadless(true));
				}
				return driver;
			default:
				System.out.print(browser + "is not supported please try other");
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			System.out.print("Check config.properties file having some issues");
			e.printStackTrace();
			return null;
		}
	}
}